import React, {useState} from 'react';
import { makeStyles, Theme, createStyles }
  from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: "30px"
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
    resetContainer: {
      padding: theme.spacing(3),
    },

  }),
);

function getSteps() {
  return [<b style={{ color: 'purple' }}>'Enter Personal Details'</b>,
  <b style={{ color: 'purple' }}>'Enter Contact Details'</b>,
  <b style={{ color: 'purple' }}>'Enter Address'</b>,
  <b style={{ color: 'purple' }}>'Enter ID'</b>];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return (
        <form class="form-group" style={{ lineHeight: "5rem" }}>
          <label>First Name</label>
          <input type="text" placeholder="First Name" style={{padding: "10px",marginLeft: "9px" }}></input>
          <br></br>
          <label>Last Name</label>
          <input type="text" placeholder="Last Name" style={{padding: "10px",marginLeft: "9px" }}></input>
        </form>
      );
    case 1:
      return (
        <form class="form-group" style={{ lineHeight: "5rem" }}>
          <label>Email Address</label>
          <input type="number" placeholder="Email Address" style={{padding: "10px",marginLeft: "9px" }}></input>
          <br></br>
          <label>Phone Number</label>
          <input type="number" placeholder="Phone number" style={{padding: "10px",marginLeft: "9px" }}></input>
        </form>
      );
    case 2:
      return (
        <form class="form-group" style={{ lineHeight: "5rem" }}>
          <label>Address</label>
          <input type="text" placeholder="Permanent Address" style={{padding: "10px",marginLeft: "9px" }}></input>
          <br></br>
          <label>Pincode</label>
          <input type="text" placeholder="Pincode" style={{padding: "10px",marginLeft: "9px" }}></input>
        </form>
      );
    case 3:
      return (
        <form class="form-group" style={{ lineHeight: "5rem" }}>
          <label>Enter ID</label>
          <input type="text" placeholder="ID-Card Number" style={{padding: "10px",marginLeft: "9px" }}></input>
          <br></br>
        </form>
      )
    default:
      return 'Unknown step';
  }
}

export default function GeekStepper() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const[greenCircle, setGreenCircle] = useState("blue")
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setGreenCircle("green")
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  }
  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <h1 style={{textAlign: "center"}}>STUDENT REGISTRATION FORM  </h1>
      <Stepper activeStep={activeStep} orientation="vertical" style={{textAlign: "center"}}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              <Typography>{getStepContent(index)}</Typography>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography style={{textAlign: "center", backgroundColor: "green", width: "200px", marginRight: "auto", marginLeft: "auto", fontFamily: "cursive", color: "white"}} variant="h6">Form is submitted</Typography>
          <Button onClick={handleReset} className={classes.button}>
            <span>Reset</span>
          </Button>
        </Paper>
      )}
    </div>
  );
}
