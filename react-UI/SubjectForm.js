import React from 'react';
import { makeStyles, Theme, createStyles }
    from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button'; 
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import SubjectIcon from "@material-ui/icons/Subject";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FaceIcon from "@material-ui/icons/Face";
import SpeedDial from '@material-ui/lab/SpeedDial';


const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            width: '100%',
            marginTop: "30px"
        },
        button: {
            marginTop: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        actionsContainer: {
            marginBottom: theme.spacing(2),
        },
        resetContainer: {
            padding: theme.spacing(3),
        },

    }),
);

const actions = [
    { icon: <FaceIcon />, name: "StudentForm" },
    { icon: <SubjectIcon />, name: "SubjectForm" }
  ];

function getSteps() {
    return [<b style={{ color: 'purple' }}>'Enter Subject ID'</b>,
    <b style={{ color: 'purple' }}>'Enter Street Name'</b>,
    <b style={{ color: 'purple' }}>'Enter City'</b>,
    ];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return (
                <form class="form-group" style={{ lineHeight: "5rem" }}>
                    <label>Subject ID</label>
                    <input type="text" placeholder="ID" style={{ padding: "10px", marginLeft: "9px" }}></input>
                    <br></br>

                </form>
            );
        case 1:
            return (
                <form class="form-group" style={{ lineHeight: "5rem" }}>
                    <label>Street</label>
                    <input type="text" style={{ padding: "10px", marginLeft: "9px" }}></input>
                    <br></br>
                </form>
            );
        case 2:
            return (
                <form class="form-group" style={{ lineHeight: "5rem" }}>
                    <label>City</label>
                    <input type="text" placeholder="City Name" style={{ padding: "10px", marginLeft: "9px" }}></input>
                    <br></br>
                </form>
            );
        case 3:
            return (
                <form class="form-group" style={{ lineHeight: "5rem" }}>
                    <label>Enter ID</label>
                    <input type="text" placeholder="ID-Card Number" style={{ padding: "10px", marginLeft: "9px" }}></input>
                    <br></br>
                </form>
            )
        default:
            return 'Unknown step';
    }
}

export default function SubjectForm() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();
    const [direction, setDirection] = React.useState("Up");
    const [open, setOpen] = React.useState(false);
    const [hidden, setHidden] = React.useState(false);

    const handleHiddenChange = (event) => {
        setHidden(event.target.checked);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };
    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
    const handleReset = () => {
        setActiveStep(0);
    };

    
    return (
        <div className={classes.root}>
            <h1 style={{ textAlign: "center" }}>SUBJECT REGISTRATION FORM  </h1>
            <Stepper activeStep={activeStep} orientation="vertical" style={{ textAlign: "center" }}>
                {steps.map((label, index) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                        <StepContent>
                            <Typography>{getStepContent(index)}</Typography>
                            <div className={classes.actionsContainer}>
                                <div>
                                    <Button
                                        disabled={activeStep === 0}
                                        onClick={handleBack}
                                        className={classes.button}
                                    >
                                        Back
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={handleNext}
                                        className={classes.button}
                                    >
                                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                    </Button>
                                </div>
                            </div>
                        </StepContent>
                    </Step>
                ))}
            </Stepper>
            {activeStep === steps.length && (
                <Paper square elevation={0} className={classes.resetContainer}>
                    <Typography style={{ textAlign: "center", backgroundColor: "green", width: "200px", marginRight: "auto", marginLeft: "auto", fontFamily: "cursive",color: "white" }} variant="h6">Form is submitted</Typography>
                    <Button onClick={handleReset} className={classes.button}>
                        Reset
                    </Button>
                </Paper>
            )}

            {/* <div className={classes.root}>
                <div className={classes.exampleWrapper}>
                    <SpeedDial
                        ariaLabel="SpeedDial example"
                        className={classes.speedDial}
                        hidden={hidden}
                        icon={<SpeedDialIcon />}
                        onClose={handleClose}
                        onOpen={handleOpen}
                        open={open}
                        direction={direction}
                    >
                        {actions.map((action) => (
                            <SpeedDialAction
                                key={action.name}
                                icon={action.icon}
                                tooltipTitle={action.name}
                                onClick={handleClose}
                            />
                        ))}
                    </SpeedDial>
                </div>
            </div> */}

        </div>
    );
}
